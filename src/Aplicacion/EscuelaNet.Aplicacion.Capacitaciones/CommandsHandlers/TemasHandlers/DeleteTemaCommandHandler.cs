﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.TemaCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers
{
    public class DeleteTemaCommandHandler : IRequestHandler<DeleteTemaCommand, CommandRespond>
    {
        private ITemaRepository _temaRepository;
        public DeleteTemaCommandHandler(ITemaRepository temaRepository)
        {
            _temaRepository = temaRepository;
        }
        public Task<CommandRespond> Handle(DeleteTemaCommand request, CancellationToken cancellationToken)
        {
            var response = new CommandRespond();
            var tema = _temaRepository.GetTema(request.Id);
            _temaRepository.Delete(tema);
            _temaRepository.UnitOfWork.SaveChanges();
            response.Succes = true;
            return Task.FromResult(response);

        }
    }
}
