﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryServices.InstructoresServices
{
    public class InstructoresQuery : IInstructoresQuery
    {
        private string _connectionString;

        public InstructoresQuery(string connectionString)
        {
            _connectionString = connectionString;
        }
        public InstructorQueryModel GetInstructor(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<InstructorQueryModel>(
                    @"
                    SELECT t.IDInstructor as Id,
                    t.Apellido as Apellido,
                    t.Nombre as Nombre,
                    t.Dni as Dni,
                    t.FechaNacimiento as FechaNacimiento
                    FROM Instructores as t
                    WHERE IDInstructor = @id
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<InstructorQueryModel> ListInstructores()
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                return connection
                   .Query<InstructorQueryModel>(
                   @"
                    SELECT t.IDInstructor as Id,
                    t.Apellido as Apellido,
                    t.Nombre as Nombre,
                    t.Dni as Dni,
                    t.FechaNacimiento as FechaNacimiento
                    FROM Instructores as t
                    "
                   ).ToList();
            }
        }
    }
}
