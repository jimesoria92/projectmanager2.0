﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Clientes.QueryModels;

namespace EscuelaNet.Aplicacion.Clientes.QueryServices
{
    public class SolicitudesQuery : ISolicitudesQuery
    {
        private string _connectionString;
        public SolicitudesQuery(string connectionString)
        {
            _connectionString = connectionString;
        }

        
        public SolicitudesQueryModel GetSolicitud(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<SolicitudesQueryModel>(
                    @"
                    SELECT s.IDSolicitud as ID, 
                    s.Titulo as Titulo,
                    s.Descripcion as Descripcion,
                    s.Estado as Estado
                    FROM Solicitudes as s
                    WHERE IDSolicitud = @id                    
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<SolicitudesQueryModel> ListSolicitud()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<SolicitudesQueryModel>(
                    @"
                   SELECT s.IDSolicitud as ID, 
                    s.Titulo as Titulo,
                    s.Descripcion as Descripcion,
                    s.Estado as Estado
                    FROM Solicitudes as s
                    "
                    ).ToList();
            }
        }

        public List<UnidadesQueryModel> ListUnidadesDeSolicitud(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<UnidadesQueryModel>(
                    @"
                    SELECT u.IDUnidadDeNegocio as ID, 
                    u.RazonSocial as RazonSocial,
                    u.ResponsableDeUnidad as ResponsableDeUnidad,
                    u.Cuit as Cuit, 
                    u.EmailResponsable as EmailResponsable,
                    u.TelefonoResponsable as TelefonoResponsable,                     
                    u.IDCliente as IdCliente, 
                    us.IDSolicitud                    
                    FROM Solicitudes as s 
					Inner JOIN UnidadSolicitud as us ON (s.IDSolicitud=us.IDSolicitud)
					Inner JOIN UnidadesDeNegocio as u ON (us.IDUnidadDeNegocio = u.IDUnidadDeNegocio)
                    WHERE s.IDSolicitud = @id  
                    ", new { id = id }
                    ).ToList();
            }
        }

        public UnidadesQueryModel FindUnidadEnSolicitud(int id, int solicitud)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<UnidadesQueryModel>(
                    @"
                    SELECT u.IDUnidadDeNegocio as ID, 
                    u.RazonSocial as RazonSocial,
                    u.ResponsableDeUnidad as ResponsableDeUnidad,
                    u.Cuit as Cuit, 
                    u.EmailResponsable as EmailResponsable,
                    u.TelefonoResponsable as TelefonoResponsable,                     
                    u.IDCliente as IdCliente, 
                    us.IDSolicitud                    
                    FROM Solicitudes as s 
					Inner JOIN UnidadSolicitud as us ON (s.IDSolicitud=us.IDSolicitud)
					Inner JOIN UnidadesDeNegocio as u ON (us.IDUnidadDeNegocio = u.IDUnidadDeNegocio)
                    WHERE s.IDSolicitud = @solicitud  AND u.IDUnidadDeNegocio= @id
                    ", new { id = id, solicitud=solicitud }
                    ).FirstOrDefault();
            }
        }


    }
}
