﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Clientes.QueryModels;

namespace EscuelaNet.Aplicacion.Clientes.QueryServices
{
    public class UnidadesQuery : IUnidadesQuery
    {
        private string _connectionString;

        public UnidadesQuery(string connectionString)
        {
            _connectionString = connectionString;
        }

        public UnidadesQueryModel GetUnidadDeNegocio(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<UnidadesQueryModel>(
                    @"
                    SELECT u.IDUnidadDeNegocio as ID, 
                    u.RazonSocial as RazonSocial,
                    u.ResponsableDeUnidad as ResponsableDeUnidad,
                    u.Cuit as Cuit, 
                    u.EmailResponsable as EmailResponsable,
                    u.TelefonoResponsable as TelefonoResponsable,                     
                    u.IDCliente as IdCliente,  
                    c.RazonSocial as RazonSocialCliente
                    FROM UnidadesDeNegocio as u 
                    INNER JOIN Clientes as c on (c.IDCliente=u.IDCliente)  
                    WHERE u.IDUnidadDeNegocio = @id                                  
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<UnidadesQueryModel> ListUnidades(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<UnidadesQueryModel>(
                    @"
                    SELECT u.IDUnidadDeNegocio as ID, 
                    u.RazonSocial as RazonSocial,
                    u.ResponsableDeUnidad as ResponsableDeUnidad,
                    u.Cuit as Cuit, 
                    u.EmailResponsable as EmailResponsable,
                    u.TelefonoResponsable as TelefonoResponsable,                     
                    u.IDCliente as IdCliente,  
                    c.RazonSocial as RazonSocialCliente
                    FROM UnidadesDeNegocio as u 
                    INNER JOIN Clientes as c on (c.IDCliente=u.IDCliente)  
                    WHERE u.IDCliente = @id     
                    ", new { id = id }
                    ).ToList();
            }
        }

        public List<SolicitudesQueryModel> ListSolicitudesDeUnidad(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<SolicitudesQueryModel>(
                    @"
                    SELECT s.IDSolicitud as ID, 
                    s.Titulo as Titulo,
                    s.Descripcion as Descripcion,
                    s.Estado as Estado
					FROM UnidadesDeNegocio AS u
                    Inner JOIN UnidadSolicitud as us ON (us.IDUnidadDeNegocio = u.IDUnidadDeNegocio)
					Inner JOIN Solicitudes as s ON (s.IDSolicitud=us.IDSolicitud)
                    WHERE u.IDUnidadDeNegocio = @id
                    ", new { id = id }
                    ).ToList();
            }
        }

        public SolicitudesQueryModel FindSolicitud(int id, int unidad)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<SolicitudesQueryModel>(
                    @"
                    SELECT s.IDSolicitud as ID, 
                    s.Titulo as Titulo,
                    s.Descripcion as Descripcion,
                    s.Estado as Estado
					FROM UnidadesDeNegocio AS u
                    Inner JOIN UnidadSolicitud as us ON (us.IDUnidadDeNegocio = u.IDUnidadDeNegocio)
					Inner JOIN Solicitudes as s ON (s.IDSolicitud=us.IDSolicitud)
                    WHERE u.IDUnidadDeNegocio = @unidad AND s.IDSolicitud=@id                                  
                    ", new { id = id , unidad=unidad }
                    ).FirstOrDefault();
            }
        }

        public List<UnidadesQueryModel> ListTodasUnidades()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<UnidadesQueryModel>(
                    @"
                    SELECT u.IDUnidadDeNegocio as ID, 
                    u.RazonSocial as RazonSocial,
                    u.ResponsableDeUnidad as ResponsableDeUnidad,
                    u.Cuit as Cuit, 
                    u.EmailResponsable as EmailResponsable,
                    u.TelefonoResponsable as TelefonoResponsable,                     
                    u.IDCliente as IdCliente,  
                    c.RazonSocial as RazonSocialCliente
                    FROM UnidadesDeNegocio as u 
                    INNER JOIN Clientes as c on (c.IDCliente=u.IDCliente)                          
                    "
                    ).ToList();
            }
        }
    }
}
