﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.CommandsHandlers
{
    public class NuevoEquipoCommandHandler : IRequestHandler<NuevoEquipoCommand, CommandRespond>
    {
        private IEquipoRepository _equipoRepositorio;
        public NuevoEquipoCommandHandler(IEquipoRepository equipoRepositorio)
        {
            _equipoRepositorio = equipoRepositorio;
        }
        public Task<CommandRespond> Handle(NuevoEquipoCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {
                    _equipoRepositorio.Add(new Equipo(request.Nombre, request.Pais, request.HusoHorario));
                    _equipoRepositorio.UnitOfWork.SaveChanges();
                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);

                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "El nombre está vacio.";
                return Task.FromResult(responde);
            }
        }
    }
}
