﻿using EscuelaNet.Aplicacion.Programadores.Responds;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.Commands
{
    public class NuevoEquipoCommand : IRequest<CommandRespond>
    {
        public string Nombre { get; set; }
        public string Pais { get; set; }
        public int HusoHorario { get; set; }
        public int CantidadProgramadores { get; set; }
        public double HorasDisponibleProgramadores { get; set; }
        //public IList<Programador> Programadores { get; set; }
        //public IList<Skills> Skills { get; set; }
    }
}
