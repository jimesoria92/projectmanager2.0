﻿using System;
using System.Collections.Generic;
using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Alumno : Persona
    {
        private Alumno()
        {

        }
        public Alumno(string nombre,string apellido,string dni, DateTime fechaNacimiento) : this()
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Apellido = apellido ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Dni = dni ?? throw new System.ArgumentNullException(nameof(nombre));
            this.FechaNacimiento = fechaNacimiento;
        }

    }
}