﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Models
{
    public class NuevoEquipoModel
    {
        public int IdEquipo { get; set; }
        public string Nombre { get; set; }
        public string Pais { get; set; }
        public int HusoHorario { get; set; }
        public int CantidadProgramadores { get; set; }
        public double HorasDisponibleProgramadores { get; set; }
        public IList<Skills> Skills { get; set; }
    }
}