﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Models
{
    public class ProgramadoresIndexModel
    {
        public string Titulo { get; set; }
        public int idEquipo { get; set; }
        public List<Programador> Programadores { get; set; }
    }
}