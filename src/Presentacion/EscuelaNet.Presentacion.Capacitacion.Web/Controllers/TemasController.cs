﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands.TemaCommand;
using EscuelaNet.Aplicacion.Capacitaciones.QueryServices;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Infraestructura.Capacitaciones;
using EscuelaNet.Infraestructura.Capacitaciones.Repositorios;
using EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura;

using EscuelaNet.Presentacion.Capacitacion.Web.Models;
using MediatR;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class TemasController : Controller
    {
        private ITemaRepository _temaRepositorio;
        private ITemasQuery _temaQuery;
        private IMediator _mediator;

        public TemasController(
            IMediator mediator,
            ITemaRepository temaRepositorio,
            ITemasQuery temaQuery)
        {
            _temaRepositorio = temaRepositorio;
            _temaQuery = temaQuery;
            _mediator = mediator;
        }

        // GET: Tema
        public ActionResult Index()
        {
            var temas = _temaQuery.ListTemas();

            var model = new TemaIndexModel()
            {
                Titulo = "Primera prueba",
                Temas = temas
            };
            return View(model);
        }

        // GET: Tema/New
        public ActionResult New()
        {
            var model = new NuevoTemaModel();
            return View(model);
        }

        // POST: Tema/New
        [HttpPost]
        public async Task<ActionResult> New(NuevoTemaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Tema creado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoTemaModel()
                {
                    Nombre = model.Nombre,
                    Nivel = model.Nivel
                };
                return View(modelReturn);
            }
        }



        // GET: Tema/Edit/5
        public ActionResult Edit(int id)
        {
            var tema = _temaQuery.GetTema(id);

            var model = new NuevoTemaModel()
            {
                Id = id,
                Nombre = tema.Nombre,
                Nivel = tema.Nivel
            };
            return View(model);
        }

        // POST: Tema2/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(UpdateTemaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Tema Editado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoTemaModel()
                {
                    Nombre = model.Nombre,
                    Nivel = model.Nivel
                };
                return View(modelReturn);
            }
        }

        // GET: Tema/Delete/5
        public ActionResult Delete(int id)
        {
            var tema = _temaQuery.GetTema(id);

            var model = new NuevoTemaModel()
            {
                Nombre = tema.Nombre,
                Id = id,
                Nivel = tema.Nivel,
            };
            return View(model);
        }

        // POST: Tema/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(DeleteTemaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Tema Elimninado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoTemaModel()
                {
                    Nombre = model.Nombre,
                    Nivel = model.Nivel
                };
                return View(modelReturn);
            }

        }
    }
}