﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class LugarIndexModel
    {
        public string Titulo { get; set; }

        public List<LugarQueryModel> Lugares { get; set; }

    }
}