﻿using EscuelaNet.Aplicacion.Clientes.QueryServices;
using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class ClientesController : Controller
    {
        private IClienteRepository _clienteRepositorio;
        private IClientesQuery _clientesQuery;

        public ClientesController(IClienteRepository clienteRepository,
            IClientesQuery clientesQuery)
        {
            _clienteRepositorio = clienteRepository;
            _clientesQuery = clientesQuery;
        }
        
        // GET: Clientes
        public ActionResult Index()
        {
            var clientes = _clientesQuery.ListCliente();

            var model = new ClientesIndexModel()
            {
                Titulo = "Index clientes",
                Clientes = clientes
            };

            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevoClienteModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevoClienteModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {
                    _clienteRepositorio.Add(new Cliente(
                            model.RazonSocial, model.Email, model.Categoria
                        ));
                    _clienteRepositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Cliente creado";
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            var cliente = _clientesQuery.GetCliente(id);
            if (cliente==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {       
                var model = new NuevoClienteModel()
                {
                    Id = id,
                    RazonSocial = cliente.RazonSocial,
                    Email = cliente.Email,
                    Categoria = cliente.Categoria
                };
            return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevoClienteModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {
                    var cliente = _clienteRepositorio.GetCliente(model.Id);
                    cliente.RazonSocial = model.RazonSocial;
                    cliente.Email = model.Email;
                    cliente.Categoria = model.Categoria;

                    _clienteRepositorio.Update(cliente);
                    _clienteRepositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Cliente editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            var cliente = _clientesQuery.GetCliente(id);
            if (cliente==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {                      
                var model = new NuevoClienteModel()
                {
                    Id = id,
                    RazonSocial = cliente.RazonSocial,
                    Email = cliente.Email,
                    Categoria = cliente.Categoria
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevoClienteModel model)
        {
            try
            {
                var cliente = _clienteRepositorio.GetCliente(model.Id);
                _clienteRepositorio.Delete(cliente);
                _clienteRepositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Cliente borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
            
        }

    }
}