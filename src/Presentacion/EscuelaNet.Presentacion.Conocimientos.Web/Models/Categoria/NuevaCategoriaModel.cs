﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class NuevaCategoriaModel
    {
        public string Descripcion { get; set; }
        public int IdCategoria { get; set; }
        public string Nombre { get; set; }
        public IList<Conocimiento> Conocimientos { get; set; }
    }
}