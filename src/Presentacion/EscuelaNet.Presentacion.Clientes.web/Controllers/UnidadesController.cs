﻿using EscuelaNet.Aplicacion.Clientes.QueryServices;
using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class UnidadesController : Controller
    {
        private IClienteRepository _clientesRepositorio;
        private ISolicitudRepository _solicitudesRepositorio;
        private IClientesQuery _clientesQuery;
        private IUnidadesQuery _unidadesQuery;
        private ISolicitudesQuery _solicitudesQuery;

        public UnidadesController(IClienteRepository clientesRepositorio,
             IClientesQuery clientesQuery,
            ISolicitudRepository solicitudesRepositorio,
            IUnidadesQuery unidadesQuery,
            ISolicitudesQuery solicitudesQuery)
        {
            _clientesRepositorio = clientesRepositorio;
            _solicitudesRepositorio = solicitudesRepositorio;
            _unidadesQuery = unidadesQuery;
            _clientesQuery = clientesQuery;
            _solicitudesQuery = solicitudesQuery; 
        }

        public ActionResult Index(int id)
        {            
            var unidades = _unidadesQuery.ListUnidades(id);
            if (unidades.Count()!=0)
            {
                var model = new UnidadesIndexModel()
                {
                    Titulo = "Unidades del Cliente '" + unidades[0].RazonSocialCliente + "'",
                    Unidades = unidades,
                    IdCliente = id
                };

                return View(model);
            }
            else
            {
                TempData["error"] = "Cliente sin unidades";
                return RedirectToAction("../Clientes/Index");
            }


        }

        public ActionResult New(int id)
        {
            var cliente = _clientesQuery.GetCliente(id);
            var model = new NuevaUnidadModel()
            {
                Titulo = "Nueva unidad para el Cliente '"+cliente.RazonSocial+"'",
                IdCliente = id
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaUnidadModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial)
                || !string.IsNullOrEmpty(model.ResponsableDeUnidad)
                || !string.IsNullOrEmpty(model.Cuit)
                || !string.IsNullOrEmpty(model.EmailResponsable)
                || !string.IsNullOrEmpty(model.TelefonoResponsable))
            {
                try
                {                    
                    var razonSocial = model.RazonSocial;
                    var responsable = model.ResponsableDeUnidad;
                    var cuit = model.Cuit;
                    var email = model.EmailResponsable;
                    var telefono = model.TelefonoResponsable;

                    var cliente = _clientesRepositorio.GetCliente(model.IdCliente);
                    
                    var unidad = new UnidadDeNegocio(razonSocial, responsable, cuit, email, telefono);
                    cliente.AgregarUnidad(unidad);
                    _clientesRepositorio.Update(cliente);
                    _clientesRepositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Unidad creada";
                    return RedirectToAction("Index/"+model.IdCliente);

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }
        
        public ActionResult Edit(int id)
        {
            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            if (unidad==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {                        

                var model = new NuevaUnidadModel()
                {
                    Titulo="Editar la Unidad '"+unidad.RazonSocial+"' del Cliente '"+ unidad.RazonSocialCliente+"'",
                    IdCliente = unidad.IdCliente,
                    IdUnidad = id,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,                                       
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevaUnidadModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial)
                || !string.IsNullOrEmpty(model.ResponsableDeUnidad)
                || !string.IsNullOrEmpty(model.Cuit)
                || !string.IsNullOrEmpty(model.EmailResponsable)
                || !string.IsNullOrEmpty(model.TelefonoResponsable))
            {
                try
                {
                    var cliente = _clientesRepositorio.GetCliente(model.IdCliente);                    
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().RazonSocial = model.RazonSocial;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().ResponsableDeUnidad = model.ResponsableDeUnidad;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().Cuit = model.Cuit;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().EmailResponsable = model.EmailResponsable;
                    cliente.Unidades.Where(un=>un.ID == model.IdUnidad).First().TelefonoResponsable = model.TelefonoResponsable;

                    _clientesRepositorio.Update(cliente);
                    _clientesRepositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Unidad editada";
                    return RedirectToAction("Index/"+model.IdCliente);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            if (unidad==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {                
                var model = new NuevaUnidadModel()
                {
                    Titulo = "Borrar la Unidad '" + unidad.RazonSocial + "' del Cliente '" + unidad.RazonSocialCliente + "'",
                    IdCliente = unidad.IdCliente,
                    IdUnidad = id,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevaUnidadModel model)
        {
            try
            {
                var unidad = _clientesRepositorio.GetUnidadDeNegocio(model.IdUnidad);
                _clientesRepositorio.DeleteUnidadDeNegocio(unidad);
                _clientesRepositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Unidad borrada";
                return RedirectToAction("../Unidades/Index/"+model.IdCliente);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }


        public ActionResult Solicitudes(int id)
        {

            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            var solicitudes = _unidadesQuery.ListSolicitudesDeUnidad(id);
            var model = new UnidadSolicitudModel()
            {
                Unidad = unidad,
                Solicitudes = solicitudes
            };

            return View(model);

        }

        public ActionResult UnlinkSolicitud(int id, int unidad)
        {
            var unidadBuscada = _unidadesQuery.GetUnidadDeNegocio(unidad);
            var solicitudBuscada = _unidadesQuery.FindSolicitud(id, unidad);

            if (unidadBuscada!=null && solicitudBuscada!=null)
            {
                var model = new NuevaUnidadSolicitudModel()
                {
                    IDSolicitud = id,
                    IDUnidad = unidad,
                    TituloSolicitud = solicitudBuscada.Titulo,
                    RazonSocialUnidad = unidadBuscada.RazonSocial
                };
                return View(model);
            }
            else
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/index");
            }

            
        }

        [HttpPost]
        public ActionResult UnlinkSolicitud(NuevaUnidadSolicitudModel model)
        {
            try
            {
                var unidadBuscada = _clientesRepositorio.GetUnidadDeNegocio(model.IDUnidad);
                var solicitudBuscada = unidadBuscada.Solicitudes.First(s => s.ID == model.IDSolicitud);

                unidadBuscada.PullSolicitud(solicitudBuscada);

                _clientesRepositorio.Update(unidadBuscada.Cliente);
                _clientesRepositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Solicitud desvinculada";
                return RedirectToAction("../Unidades/Solicitudes/"+model.IDUnidad);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult LinkSolicitud(int id)
        {
            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            var solicitudes = _solicitudesQuery.ListSolicitud();

            var model = new NuevaUnidadSolicitudModel()
            {
                IDUnidad = id,
                RazonSocialUnidad = unidad.RazonSocial,
                Solicitudes = solicitudes
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult LinkSolicitud(NuevaUnidadSolicitudModel model)
        {
                       
            try
            {
                var unidadBuscada = _clientesRepositorio.GetUnidadDeNegocio(model.IDUnidad);

                var solicitudBuscada = _solicitudesRepositorio.GetSolicitud(model.IDSolicitud);

                var bandera = true;
                var bandera2 = true;

                foreach (var unidadLista in solicitudBuscada.UnidadesDeNegocio)
                {
                    if (unidadLista.IDCliente != unidadBuscada.Cliente.ID )
                    {
                        bandera = false;
                    }
                    if (unidadLista.ID == model.IDUnidad)
                    {
                        bandera2 = false;
                    }
                }
                if (bandera && bandera2)
                {

                    unidadBuscada.AgregarSolicitud(solicitudBuscada);

                    _clientesRepositorio.Update(unidadBuscada.Cliente);
                    _clientesRepositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Solicitud vinculada";
                    return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);

                }
                else if(bandera2)
                {
                    TempData["error"] = "Solicitud vinculada a una unidad de otro cliente";
                    return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);
                }
                else
                {
                    TempData["error"] = "Solicitud ya vinculada a esta Unidad";
                    return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);
            }

        }


    }
}