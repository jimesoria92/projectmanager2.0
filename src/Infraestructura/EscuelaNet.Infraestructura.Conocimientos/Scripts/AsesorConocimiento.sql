﻿USE [AdministradorDeProyectos]
GO

/****** Object:  Table [dbo].[AsesorConocimiento]    Script Date: 15/9/2019 20:36:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AsesorConocimiento](
	[IDAsesor] [int] NOT NULL,
	[conocimiento_ID] [int] NOT NULL,
 CONSTRAINT [PK_AsesorConocimiento] PRIMARY KEY CLUSTERED 
(
	[IDAsesor] ASC,
	[conocimiento_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO