﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using EscuelaNet.Dominio.SeedWoork;
using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Infraestructura.Proyectos.EntityTypeConfigurations;

namespace EscuelaNet.Infraestructura.Proyectos
{
    public class LineaContext : DbContext, IUnitOfWork
    {
        public DbSet<LineaDeProduccion> LineasDeProduccion { get; set; }
        public DbSet<Proyecto> Proyectos { get; set; }
        public DbSet<Etapa> Etapas { get; set; }

        public LineaContext() : base("LineaContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new LineaEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new ProyectoEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new EtapaEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }

    }
}
